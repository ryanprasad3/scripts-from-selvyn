echo off
echo Starting MySQL, root password=ppp
docker run -p 3307:3306 --name mysql-dbda -e MYSQL_ROOT_PASSWORD=ppp -d -v C:\vm_share\mysql:/root mysql-db-unpopulated:latest
echo MySQL container now running
echo 

echo Waiting for MySQL deamon to initialise, do not interrupt... (CTRL+C will termiate the install)
timeout 30 /NOBREAK

echo Initialising and populating the DB, can take approximately 5 minutes to complete, please wait...
docker exec -it mysql-dbda bash -c "cd /root; ./init-populate-mysql-db.sh"
echo MySQL available for use...
echo  
