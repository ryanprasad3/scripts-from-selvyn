echo on

copy %1.war c:\vm_share\tomcat-webapps
timeout 30 /NOBREAK

docker cp dbConnector.properties t9s-dbda-91:/usr/local/tomcat/webapps/%1/WEB-INF/classes
docker cp dbConnector.properties t9s-dbda-92:/usr/local/tomcat/webapps/%1/WEB-INF/classes

