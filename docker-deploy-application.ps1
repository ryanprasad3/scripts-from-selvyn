﻿
function displayHelp
{
    Write-Host("`nusage: docker-deploy-application.ps1 -warfile <warfile name> [other options]" );
    Write-Host("`nother optons"); 
    Write-Host("`t-help       `tdisplay this usage message"); 
    Write-Host("`t-properties `t<property file> default dbDonnector.properties"); 
    Write-Host("`t-servers    `t<#> number of servers to deploy to"); 
    Write-Host("`ne.g. docker-deploy-application.ps1 -warfile DBankWebTier -servers 3 -properties dbConnector.properties`n`n");
}


$instance_id = 0;
$server_name = "tomcat-server-runtime_";
$command_to_run_lh = "docker cp ";
$webapps_path = ":/opt/tomcat/webapps";
$web_inf_path_lh = ":/opt/tomcat/webapps/";
$web_inf_path_rh = "/WEB-INF/classes";
$command_to_run = "";
$index = 0;
$warfile_name = "";
$server_count = 1;
$warfile_name = "DBankWebTier";
$propsfile_name = "";
$helpShown = $false;

foreach( $element in $args )
{
    $param = $args[$index];
    if( $param )
    {
        if( "-help".Equals( $args[$index].ToString().ToLower() ) )
        {
            displayHelp;
            $helpShown = $true;
        }

        if( "-servers".Equals( $args[$index].ToString().ToLower() ) )
        {
            $server_count = [Int]$args[$index+1];
            #Write-Host $server_count;
        }

        if( "-warfile".Equals( $args[$index].ToString().ToLower() ) )
        {
            $warfile_name = $args[$index+1];
            #Write-Host $warfile_name;
        }

        if( "-properties".Equals( $args[$index].ToString().ToLower() ) )
        {
            $propsfile_name = $args[$index+1];
            #Write-Host $propsfile_name;
        }
    }
    $index++;
}

Write-Host ("`n");

if( $args.Count -gt 1 )
{
    foreach( $instance_id in 1..$server_count )
    {
        $command_to_run = $command_to_run_lh + 
                            $warfile_name + ".war " + 
                            $server_name + 
                            $instance_id + 
                            $webapps_path;
        Write-Host( "executing ==> " + $command_to_run );
        iex $command_to_run;
    }

    if( $propsfile_name.ToString().Length -gt 0 )
    {
        foreach( $instance_id in 1..$server_count )
        {
            $command_to_run = $command_to_run_lh + 
                                $propsfile_name + " " + 
                                $server_name + 
                                $instance_id + 
                                $web_inf_path_lh + 
                                $warfile_name + 
                                $web_inf_path_rh;
            Write-Host( "executing ==> " + $command_to_run );
            iex $command_to_run;
        }
    }
}
else
{
    if( $helpShown -ne $true )
    {
        displayHelp;
    }
}
