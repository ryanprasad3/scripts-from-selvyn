echo off
echo Starting tomcat container, can take approximately 1 minute to complete, please wait...
docker run -p 8091:8080 -dit --name t9s-dbda-91 -v c:\vm_share\tomcat-webapps\dbanalyzer.war:/usr/local/tomcat/webapps/dbanalyzer.war tomcat9-server
docker run -p 8092:8080 -dit --name t9s-dbda-92 -v c:\vm_share\tomcat-webapps/dbanalyzer.war:/usr/local/tomcat/webapps/dbanalyzer.war tomcat9-server
timeout 30 /NOBREAK

echo ..
echo ..
echo tomcat should now be available
echo
